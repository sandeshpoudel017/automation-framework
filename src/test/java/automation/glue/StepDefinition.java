package automation.glue;

import automation.config.AutomationFrameworkConfiguration;
import automation.drivers.DriverSingleton;
import automation.pages.CheckoutPage;
import automation.pages.HomePage;
import automation.pages.SignInPage;
import automation.utils.*;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.commons.logging.Log;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@ContextConfiguration(classes = AutomationFrameworkConfiguration.class)
public class StepDefinition {
    private WebDriver driver;
    private HomePage homePage;
    private SignInPage signInPage;
    private CheckoutPage checkoutPage;

    private FrameworkProperties frameworkProperties;
    ExtentTest test;
    static ExtentReports report = new ExtentReports("reports/TestReport.html");

    @Autowired
    ConfigurationProperties configurationProperties;

    @Before
    public void initializeObjects(){
        //DriverSingleton.getInstance(configurationProperties.getBrowser());
        DriverSingleton.getInstance(Constants.CHROME);
        homePage = new HomePage();
        signInPage = new SignInPage();
        checkoutPage = new CheckoutPage();
        frameworkProperties = new FrameworkProperties();
        TestCases[] tests = TestCases.values();
        test = report.startTest(tests[Utils.testCount].getTestName());
        Utils.testCount++;
    }

    @Given("^I go to the Website")
    public void i_go_to_the_Website(){
        driver = DriverSingleton.getDriver();
        driver.get(Constants.URL);
        test.log(LogStatus.PASS, "Navigating to " + Constants.URL);
    }

    @When("^I click on Sign In button")
    public void i_click_on_sign_in_button(){
        homePage.clickSignIn();
        test.log(LogStatus.PASS, "SignIn Button is clicked");
    }

    @When("^I add two elements to the cart")
    public void i_add_two_elements_to_the_cart() {
        homePage.addFirstElementToCart();
        homePage.addSecondElementToCart();
        test.log(LogStatus.PASS, "Two elements were added to the cart");
    }

    @And("^I specify my credentials and click Login")
    public void i_specify_my_credentials_and_click_login(){
        //signInPage.logIn(configurationProperties.getEmail(), configurationProperties.getPassword());
        signInPage.logIn(frameworkProperties.getProperty(Constants.EMAIL),frameworkProperties.getProperty(Constants.PASSWORD));
        test.log(LogStatus.PASS, "Login has been clicked.");
    }

    @And("^I proceed to checkout")
    public void i_proceed_to_checkout(){
        checkoutPage.goToCheckout();
        test.log(LogStatus.PASS, "We proceed to checkout");
    }

    @And("^I confirm address, shipping, payment and final order")
    public void i_confirm_address_shipping_payment_and_final_order(){
        checkoutPage.confirmAddress();
        checkoutPage.confirmShipping();
        checkoutPage.payByBankWire();
        checkoutPage.confirmFinalOrder();
        test.log(LogStatus.PASS, "We confirm the final order");
    }

    @Then("^I can log into the website")
    public void i_can_log_into_the_website(){
        //assertEquals(configurationProperties.getUserName(), homePage.getUserName());
        if(frameworkProperties.getProperty(Constants.USERNAME).equals(homePage.getUserName())){
            test.log(LogStatus.PASS, "The Authentication is Successful");
        }else{
            test.log(LogStatus.FAIL, "The Authentication is not successful");
        }
        assertEquals(frameworkProperties.getProperty(Constants.USERNAME),homePage.getUserName());
    }

    @Then("^The elements are bought")
    public void the_elements_are_bought(){
        if(checkoutPage.checkFinalStatus())
            test.log(LogStatus.PASS, "The two items are bought.");
        else
            test.log(LogStatus.FAIL, "The items weren't bought");
        assertTrue(checkoutPage.checkFinalStatus());
    }
    @After
    public void closeObjects(){
        report.endTest(test);
        report.flush();
        DriverSingleton.closeObjectInstance();
    }
}
